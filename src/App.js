import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
  
} from "react-router-dom";
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import MovieList from './components/MovieList.jsx';
import MovieDetail from './components/MovieDetail.jsx';

import { createMuiTheme } from '@material-ui/core/styles';



export default function App() {
  return (    
    <Router>
      <AppBar position="static">
        <Toolbar>          
          <Typography variant="h6">
            <Link to="/">Movie List</Link>
          </Typography>         
        </Toolbar>
      </AppBar>
      <div> 

        <Switch>
          <Route exact path="/">
            <MovieList />
          </Route>
          <Route path="/movie/:id" children={<MovieDetail />} />
        </Switch>
      </div>
    </Router>
  );
}
