import React, { useEffect, useState } from "react";

import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '80%',
    height: 'auto',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));

const MovieList = (props) => {

  const [movieListData, setMovieListData] = useState([]);

  useEffect(() => {
    fetch(`http://127.0.0.1:5000/movie/list`)
      .then(res => res.json())
      .then(data => {
        setMovieListData(data);
      })
      .catch(err => console.error(err));
  },[]); 

  const classes= useStyles();

  return (
    <div className={classes.root}>
      <GridList cellHeight={240} className={classes.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
          <ListSubheader component="div">Star Trek Movies</ListSubheader>
        </GridListTile>
        {movieListData.map((movie) => (
          <GridListTile key={movie.moviePoster}>
            <img src={movie.moviePoster} alt={movie.movieName} />
            <GridListTileBar
              title={movie.movieTitle}              
              actionIcon={
                <a href={`/movie/${movie.id}`}><IconButton aria-label={`info about ${movie.MovieName}`} className={classes.icon}>
                  <InfoIcon />
                </IconButton></a>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
  

  // return <div><ul>{movieListData.map(movie => <li key={movie.id}><img src={movie.moviePoster} /><br /><a href={`/movie/${movie.id}`}>{movie.movieTitle}</a></li>)}</ul></div>
  
}

export default MovieList;