import flask
from flask import jsonify
from flask_cors import CORS, cross_origin
import requests
import json
from types import SimpleNamespace

app = flask.Flask(__name__)
app.config["DEBUG"] = True
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

sampleMovieDetail = {
  "id": "tt0110357",
  "movieTitle": "The Lion King",
  "movieYear": "1994",
  "movieLength": "88 min",
  "moviePlot": "Lion prince Simba and his father are targeted by his bitter uncle, who wants to ascend the throne himself.",
  "moviePoster": "https://m.media-amazon.com/images/M/MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_SX300.jpg",
  "contentType": "movie"
}

def movieApiEncoder(movieObj):
    return movieObj.__dict__

class Movie:
   def __init__(self, id, movie_title, movie_year, movie_length = None, movie_poster=None, movie_plot=None):
      self.contentType="movie"
      self.id = id
      self.movieTitle = movie_title
      self.movieYear = movie_year
      self.movieLength = movie_length
      self.moviePoster = movie_poster
      self.moviePlot = movie_plot


apiUrl = "http://www.omdbapi.com/?"
#TODO pass this in as a secret
apiKey="34e5edd6"
#ombd paginates at 10 results and gives total results, so you can build and paginate that way
#naive, we will pull everything and return it - maybe have time to paginate

@app.route('/movie/list', methods=['GET'])
@cross_origin()
def movie_list():
    page=1
    results=[]

    apiResponse = requests.get(f'{apiUrl}&s=Star+Trek&page={page}&apiKey={apiKey}').json()
    while apiResponse["Response"]=="True":
        page+=1
        for movie in apiResponse["Search"]:
            
            ## don't really need this but I like it for the dot notation, could just directly access dict
            movieObj = SimpleNamespace(**movie)
            translatedMovie = Movie(id = movieObj.imdbID, movie_title = movieObj.Title, movie_year = movieObj.Year, movie_poster = movieObj.Poster)
            
            results.append(movieApiEncoder(translatedMovie))

        apiResponse = requests.get(f'{apiUrl}&type=movie&s=Star+Trek&page={page}&apiKey={apiKey}').json()
        print(apiResponse["Response"])

    if not results:
        return "No movies found", 404

    return jsonify(results)



@app.route('/movie/detail/<string:movie_id>', methods=['GET'])
@cross_origin()
def movie_detail(movie_id):   

    apiResponse = requests.get(f'{apiUrl}&i={movie_id}&apiKey={apiKey}').json()
    translatedMovie = None

    if apiResponse["Response"] == "True":

        movieObj = SimpleNamespace(**apiResponse)
        translatedMovie = Movie( id = movieObj.imdbID, movie_title = movieObj.Title, 
        movie_year=  movieObj.Year, movie_length =  movieObj.Runtime, movie_poster =  movieObj.Poster,
        movie_plot = movieObj.Plot)

        return jsonify(movieApiEncoder(translatedMovie))

    return "Movie not found", 404

app.run()